//
//  CoursesListPresenter.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 13/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import Foundation
import RealmSwift

protocol CoursesListPresenterProtocol {
    func requestCoursesNextPage()
    func refreshCourses()
}

class CoursesListPresenter: CoursesListPresenterProtocol {
    weak var view: CoursesListViewProtocol!
    
    var token: NotificationToken?
    
    var currentPage = 0
    var hasNextPage: Bool = true
    
    //MARK: CoursesListPresenterProtocol
    func requestCoursesNextPage() {
        requestCoursesNextPage(needToDeleteOld: false)
    }
    
    func requestCoursesNextPage(needToDeleteOld: Bool) {
        DispatchQueue.global().async { [weak self] in
            guard let sself = self else { return }
            
            if sself.hasNextPage && !NetworkManager.shared.coursesIsRequestingNow {
                _ = NetworkManager.shared.requestCourses(page: sself.currentPage + 1, needToDeleteOld: needToDeleteOld, handlePageFlags: sself.handlePageFlags(hasNext:hasPrevious: currentPage:))
            }
        }
        
        token?.stop()
        let realm = try! Realm()
        let results = realm.objects(Course.self)
        
        token = results.addNotificationBlock({ [weak self] (changes: RealmCollectionChange) in
            switch changes {
            case .initial(let courses):
                // Results are now populated and can be accessed without blocking the UI
                self?.present(courses: courses)
                
            case .update(let courses, let deletions, let insertions, let modifications):
                // Query results have changed, so apply them to the UITableView
                self?.update(courses: courses, insertions: insertions.map({ IndexPath(row: $0, section: 0) }), deletions: deletions.map({ IndexPath(row: $0, section: 0)}), modifications: modifications.map({ IndexPath(row: $0, section: 0) }))
                
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                errorPrint("\(error)")
            }
        })
    }
    
    @objc func handlePageFlags(hasNext: Bool, hasPrevious: Bool, currentPage: Int) {
        hasNextPage = hasNext
        self.currentPage = currentPage
        debugPrint("currentPage: \(currentPage)")
    }
    
    func refreshCourses() {
        token?.stop()
        currentPage = 0
        requestCoursesNextPage(needToDeleteOld: true)
    }
    
    //MARK: usefull funcs
    func present(courses: Results<Course>) {
        DispatchQueue.main.async {
            self.view.display(coursesModels: self.buildCoursesModel(from: courses))
        }
    }
    
    func update(courses: Results<Course>, insertions: [IndexPath], deletions: [IndexPath], modifications: [IndexPath]) {
        let coursesModel = buildCoursesModel(from: courses)
        DispatchQueue.main.async {
            self.view.update(coursesModels: coursesModel, insertions: insertions, deletions: deletions, modifications: modifications)
        }
    }
    
    private func buildCoursesModel(from courses: Results<Course>) -> CoursesListViewModel {
        var coursesModels = [CourseShortViewModel]()
        for course in courses {
            var url: URL?
            if let coverUrl = course.coverUrl {
                url = URL(string: coverUrl)
            }
            coursesModels.append(CourseShortViewModel(id: course.id, title: course.title ?? "noTitle", description: course.descNoTags ?? "noDescription", cover: url, startDate: course.beginDate, endDate: course.endDate))
        }
        return CoursesListViewModel(courses: coursesModels, hasNextPage: hasNextPage)
    }
    
    deinit {
        token?.stop()
    }
}
