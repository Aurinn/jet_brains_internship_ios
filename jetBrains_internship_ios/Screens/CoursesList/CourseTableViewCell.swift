//
//  CourseTableViewCell.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 14/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import UIKit
import SDWebImage

class CourseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var datesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    private func setupCell() {
        titleLabel.font = UIFont.textSemibold15PtFont
        descriptionLabel.font = UIFont.textRegular13PtFont
        datesLabel.font = UIFont.textRegular12PtFont
        coverImageView.contentMode = .scaleAspectFit
    }
    
    func bind(viewModel: CourseShortViewModel) {
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.description
        
        if let startDate = viewModel.startDate, let endDate = viewModel.endDate {
            datesLabel.text = "\(startDate.dayWithMonthAndYear) - \(endDate.dayWithMonthAndYear)"
        } else { datesLabel.text = nil }
        
        if let url = viewModel.cover {
            coverImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "course.coverPlaceholder"))
        } else {
            coverImageView.image = UIImage(named: "course.coverPlaceholder")
        }
    }
}
