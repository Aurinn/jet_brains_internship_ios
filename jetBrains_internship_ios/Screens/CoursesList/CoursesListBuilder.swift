//
//  CoursesListBuilder.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 14/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import UIKit

class CoursesListBuilder {
    
    class func buildControllerForDefault() -> CoursesListViewController {
        let controller = CoursesListViewController()
        let presenter = CoursesListPresenter()
        
        presenter.view = controller
        controller.presenter = presenter
        
        return controller
    }
}
