//
//  String.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 14/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import UIKit

extension String {
    var stringFromHtml: NSAttributedString? {
        do {
            let data = self.data(using: String.Encoding.unicode, allowLossyConversion: true)
            if let d = data {
                let resultString: NSAttributedString = try NSAttributedString(data: d,
                                                 options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                 documentAttributes: nil)
                return resultString
            }
        } catch(let error) {
            errorPrint("error: \(error)")
        }
        return nil
    }
    
    var noTagString: String? {
        return stringFromHtml?.string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}
