//
//  UIColor.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 13/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import UIKit

extension UIColor {
    static var stepikGreen: UIColor {
        //#colorLiteral(red: 0.137254902, green: 0.8431372549, blue: 0.4, alpha: 1)
        return UIColor(red: 102 / 255.0, green: 204 / 255.0, blue: 102 / 255.0, alpha: 1)
    }
}
