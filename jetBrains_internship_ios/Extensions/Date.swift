//
//  Date.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 14/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import Foundation

extension Date {
    var dayWithMonthAndYear: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMMM y"
        //TODO: set locale to choose
        //        dateFormatter.locale =
        
        return dateFormatter.string(from: self)
    }
}
