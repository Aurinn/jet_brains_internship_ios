//
//  UIFont.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 14/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import UIKit

extension UIFont {
    /**
     Guideline style - Title 2
     */
    static var displayRegular22PtFont: UIFont {
        return UIFont.systemFont(ofSize: 22.0, weight: UIFontWeightRegular)
    }
    
    /**
     Guideline style - Title 3
     */
    static var displayRegular20PtFont: UIFont {
        return UIFont.systemFont(ofSize: 20.0, weight: UIFontWeightRegular)
    }
    
    /**
     Guideline style - Headline
     */
    static var textSemibold17PtFont: UIFont {
        return UIFont.systemFont(ofSize: 17.0, weight: UIFontWeightSemibold)
    }
    
    static var textSemibold15PtFont: UIFont {
        return UIFont.systemFont(ofSize: 15.0, weight: UIFontWeightSemibold)
    }
    
    /**
     Guideline style - Body
     */
    static var textRegular17PtFont: UIFont {
        return UIFont.systemFont(ofSize: 17.0, weight: UIFontWeightRegular)
    }
    
    /**
     Guideline style - Callout
     */
    static var textRegular16PtFont: UIFont {
        return UIFont.systemFont(ofSize: 16.0, weight: UIFontWeightRegular)
    }
    
    /**
     Guideline style - Subhead
     */
    static var textRegular15PtFont: UIFont {
        return UIFont.systemFont(ofSize: 15.0, weight: UIFontWeightRegular)
    }
    
    /**
     Guideline style - Footnote
     */
    static var textRegular13PtFont: UIFont {
        return UIFont.systemFont(ofSize: 13.0, weight: UIFontWeightRegular)
    }
    
    /**
     Guideline style - Caption 1
     */
    static var textRegular12PtFont: UIFont {
        return UIFont.systemFont(ofSize: 12.0, weight: UIFontWeightRegular)
    }
    
    /**
     Guideline style - Caption 2
     */
    static var textRegular11PtFont: UIFont {
        return UIFont.systemFont(ofSize: 11.0, weight: UIFontWeightRegular)
    }
}
