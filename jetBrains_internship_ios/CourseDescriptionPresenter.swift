//
//  CourseDescriptionPresenter.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 14/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import Foundation
import RealmSwift

protocol CoursesDescriptionPresenterProtocol {
    func requestCourseinfo(id: Int)
}

class CourseDescriptionPresenter: CoursesDescriptionPresenterProtocol {
    weak var view: CoursesDescriptionViewProtocol!
    
    var token: NotificationToken?
    
    //MARK: CoursesDescriptionPresenterProtocol
    func requestCourseinfo(id: Int) {
        //TODO: add request to server
        
        token?.stop()
        let realm = try! Realm()
        let results = realm.objects(Course.self).filter("id = %d", id)
        
        token = results.addNotificationBlock({ [weak self] (changes: RealmCollectionChange) in
            switch changes {
            case .initial(let courses):
                self?.present(course: courses.first)
                
            case .update(let courses, _, _, _):
                self?.present(course: courses.first)
                
            case .error(let error):
                errorPrint("\(error)")
            }
        })
    }
    
    func present(course: Course?) {
        guard let course = course else { return }
        
        var videoUrl: URL?
        if let videoUrlString = course.introVideo?.urls.first?.url {
            videoUrl = URL(string: videoUrlString)
        }
        var coverUrl: URL?
        if let coverString = course.introVideo?.thumbnail {
            coverUrl = URL(string: coverString)
        }
        
        var instructors: [CourseViewModel.InstructorsModel] = []
        for instructor in [course.instructors ?? ""] {//TODO: fix model in db
            instructors.append(CourseViewModel.InstructorsModel(name: instructor, photoUrl: nil, description: nil))
        }
        var detailed = [(String, String)]()
        if let desc = course.descNoTags {
            detailed.append(("Description", desc))
        }
        if let workload = course.workload {
            detailed.append(("Workload", workload))
        }
        if let certificate = course.certificate {
            detailed.append(("Certificate", certificate))
        }
        if let audience = course.targetAudience {
            detailed.append(("Audience", audience))
        }
        if let format = course.courseFormat {
            detailed.append(("Format", format))
        }
        if let requirements = course.requirementsNoTags {
            detailed.append(("Requirements", requirements))
        }
        
        view.display(course: CourseViewModel(videoUrl: videoUrl,
                                             coverUrl: coverUrl,
                                             title: course.title,
                                             summary: course.summaryNoTags,
                                             instructors: instructors,
                                             detailed: detailed,
                                             syllabus: ["no plan yet"]))
    }
    
    deinit {
        token?.stop()
    }
}
