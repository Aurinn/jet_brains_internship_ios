//
//  NetworkManager.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 12/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

enum StepikService: String {
    case service = "https://stepik.org:443"
    case courses = "/api/courses"
    
    var url: String {
        return rawValue
    }
}

class NetworkManager {
    static let shared = NetworkManager()
    var coursesIsRequestingNow = false
    
    func requestCourses(page: Int, needToDeleteOld: Bool = false, handlePageFlags: ((Bool, Bool, Int) -> ())? = nil) -> DataRequest? {
        let path = StepikService.service.url + StepikService.courses.url
        coursesIsRequestingNow = true
        return requestMaker(with: path,
                            params: ["page": page, "exclude_ended": true, "is_public": true, "order": "-activity"],
                            success: { [weak self] (json) in
                                self?.coursesIsRequestingNow = false
                                if let coursesDto = Mapper<CoursesDto>().map(JSONString: json) {
                                    if let meta = coursesDto.meta {
                                        handlePageFlags?(meta.hasNext, meta.hasPrevious, meta.page)
                                    }
                                    if let courses = coursesDto.courses {
                                        DatabaseManager.shared.addToDatabase(courses: courses, needToDeleteOld: needToDeleteOld)
                                    }
                                }
            }, fail: { [weak self] (error) in
                self?.coursesIsRequestingNow = false
        })
    }
    
    //MARK: request maker
    private func requestMaker(with path: String, params: [String : Any], senderName: String = #function,
                              success: ((String) -> Void)?, fail: ((Error) -> Void)? = nil) -> DataRequest? {
        
        return Alamofire.request(path, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .validate()
            .responseString(completionHandler: { (response: DataResponse<String>) in
                switch(response.result) {
                case .success(let jsonString):
                    debugPrint("request suceeded: \(path), params: \(params)")
                    success?(jsonString)
                case .failure(let error):
                    errorPrint("request failed: \(path)")
                    fail?(error)
                }
            })
    }
}
