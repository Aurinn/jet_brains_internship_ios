//
//  DatabaseManager.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 13/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import Foundation
import RealmSwift

class DatabaseManager {
    static let shared = DatabaseManager()
    
    func addToDatabase(courses: [Course], needToDeleteOld: Bool = false) {
        DispatchQueue.global().async {
            let realm = try! Realm()
            
            do {
                try realm.write {
                    for course in courses {
                        if var dbCourse = realm.objects(Course.self).filter("id == %d", course.id).first {
                            if dbCourse != course {
                                dbCourse = course
                            }
                        } else {
                            realm.add(course)
                        }
                    }
                    if needToDeleteOld {
                        let newIds = courses.map({ $0.id })
                        let toDelete = realm.objects(Course.self).filter("NOT id IN %@", newIds)
                        realm.delete(toDelete)
                    }
                }
            } catch let error {
                fatalError("\(error)")
            }
        }
    }
    
    func deleteAllCourses() {
        DispatchQueue.global().async {
            let realm = try! Realm()
            do {
                try realm.write {
                    realm.delete(realm.objects(Course.self))
                }
            } catch {
                
            }
        }
    }
}
