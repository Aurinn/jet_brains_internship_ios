//
//  CourseDescriptionBuilder.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 14/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import Foundation

class CourseDescriptionBuilder {
    class func buildControllerForDefault(with courseId: Int = 0) -> CourseDescriptionViewController {
        let controller = CourseDescriptionViewController(courseId: courseId)
        let presenter = CourseDescriptionPresenter()
        
        presenter.view = controller
        controller.presenter = presenter
        
        return controller
    }
}
