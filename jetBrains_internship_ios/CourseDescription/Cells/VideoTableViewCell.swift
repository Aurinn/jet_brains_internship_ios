//
//  VideoTableViewCell.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 14/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import UIKit
import AVFoundation
import SDWebImage

struct VideoCellViewModel {
    let videoItem: AVPlayerItem?
    let placeholderImage: URL?
}

class VideoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var playerContentView: UIView!
    @IBOutlet weak var playButton: PlayButton!
    @IBOutlet weak var placeholderImageView: UIImageView!
    
    var player: AVPlayer = AVPlayer()
    var playerLayer: AVPlayerLayer!
    var isPaused: Bool = true
 
    var videoPlayerItem: AVPlayerItem? = nil {
        didSet {
            player.replaceCurrentItem(with: videoPlayerItem)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupVideoPlayer()
        placeholderImageView.contentMode = .scaleAspectFit
        placeholderImageView.backgroundColor = .white
    }
    
    private func setupVideoPlayer() {
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
        
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        player.actionAtItemEnd = .none
        
        playerContentView.frame = contentView.frame
        playerLayer.frame = contentView.frame
        backgroundColor = .clear
        playerContentView.layer.insertSublayer(playerLayer, at: 0)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tappedInVideoArea(sender:)))
        playerContentView.addGestureRecognizer(tapRecognizer)
        
        //to handle the end of the video
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: player.currentItem)
    }
    
    func bind(model: VideoCellViewModel) {
        videoPlayerItem = model.videoItem
        
        if let url = model.placeholderImage {
            placeholderImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "course.coverPlaceholder"))
        } else {
            placeholderImageView.image = UIImage(named: "course.coverPlaceholder")
        }
    }
    
    func stopPlayback() {
        player.pause()
        isPaused = true
    }
    
    func startPlayback() {
        player.play()
        isPaused = false
    }
    
    @IBAction func playButtonPressed(_ sender: UIButton) {
        if isPaused {
            startPlayback()
            playButton.isHidden = true
            placeholderImageView.isHidden = true
        }
    }
    
    func tappedInVideoArea(sender: UITapGestureRecognizer) {
        if isPaused {
            playButtonPressed(playButton)
        } else {
            stopPlayback()
            playButton.isHidden = false
            placeholderImageView.isHidden = false
        }
    }
    
    func playerItemDidReachEnd(notification: Notification) {
        if let playerItem = notification.object as? AVPlayerItem {
            //sent to the beginning to loop the video again
            playerItem.seek(to: kCMTimeZero)
        }
    }
    
    //MARK: layout
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        playerLayer.frame = playerContentView.frame
    }
}
