//
//  CourseSectionHeader.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 15/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import UIKit

protocol CourseSectionHeaderDelegate {
    func segmentedControllValueChanged(sender: CourseSectionHeader, segmentedControll: UISegmentedControl)
}

class CourseSectionHeader: UIView {

    var delegate: CourseSectionHeaderDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBAction func segmentedControlvalueChanged(_ sender: UISegmentedControl) {
        delegate?.segmentedControllValueChanged(sender: self, segmentedControll: sender)
    }
}
