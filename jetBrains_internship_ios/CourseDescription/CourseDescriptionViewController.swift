//
//  CourseDescriptionViewController.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 14/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import UIKit
import AVFoundation

struct CourseViewModel {
    struct InstructorsModel {
        let name: String?
        let photoUrl: Url?
        let description: String?
    }
    
    let videoUrl: URL?
    let coverUrl: URL?
    let title: String?
    let summary: String?
    let instructors: [InstructorsModel]?
    
    let detailed: [(String, String)]?
    let syllabus: [String]?
}

enum CourseSegments: Int {
    case overview = 0
    case detailed = 1
    case syllabus = 2
}

protocol CoursesDescriptionViewProtocol: class {
    func display(course: CourseViewModel)
}

class CourseDescriptionViewController: UIViewController, CoursesDescriptionViewProtocol {
    var presenter: CoursesDescriptionPresenterProtocol?
    
    var courseId: Int
    var viewModel: CourseViewModel?
    var currentSegment: CourseSegments = .overview
    
    internal var videoCell: VideoTableViewCell?
    
    internal let tableView = UITableView()
    internal let videoCellIdentifier = "video_cell"
    internal let titleTextCellIdentifier = "title_cell"
    internal let instructorsCellIdentifier = "instructors_cell"
    internal let textCellIdentifier = "text_cell"
    
    init(courseId: Int) {
        self.courseId = courseId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        courseId = 0
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "VideoTableViewCell", bundle: nil), forCellReuseIdentifier: videoCellIdentifier)
        tableView.register(TitleTextTableViewCell.self, forCellReuseIdentifier: titleTextCellIdentifier)
        //TODO: register cells
        
        presenter?.requestCourseinfo(id: courseId)
    }
    
    private func setupView() {
        view.addSubview(tableView)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 180
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.estimatedSectionHeaderHeight = 100
    }
    
    //MARK: CoursesDescriptionViewProtocol
    func display(course: CourseViewModel) {
        viewModel = course
        tableView.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        videoCell?.stopPlayback()
    }
}

extension CourseDescriptionViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.numberOfSections > 1 && indexPath.section == 0 {
            return 192
        }
        if currentSegment == .overview && indexPath.row == 1 {
            return 180
        }
        return UITableViewAutomaticDimension
    }
}

extension CourseDescriptionViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.numberOfSections > 1 && section == 0 {
            return 1
        }
        var result = 0
        switch currentSegment {
        case .overview:
            result = 2
        case .detailed:
            if let detailed = viewModel?.detailed {
                result = detailed.count
            }
        case .syllabus:
            if let syllabus = viewModel?.syllabus {
                result = syllabus.count
            }
        }
        return result
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.videoUrl == nil ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        if tableView.numberOfSections > 1 && indexPath.section == 0 { //video
            if let videoCell = videoCell { //TODO: check video url
                return videoCell
            }
            
            if let newVideoCell = tableView.dequeueReusableCell(withIdentifier: videoCellIdentifier, for: indexPath) as? VideoTableViewCell, let url = viewModel?.videoUrl {
                newVideoCell.bind(model: VideoCellViewModel(videoItem: AVPlayerItem(url: url), placeholderImage: viewModel?.coverUrl))
                cell = newVideoCell
                videoCell = newVideoCell
            }
        } else {
            switch currentSegment {
            case .overview:
                if indexPath.row == 0 { //summary
                    if let titleCell = tableView.dequeueReusableCell(withIdentifier: titleTextCellIdentifier, for: indexPath) as? TitleTextTableViewCell {
                        titleCell.bind(title: "Summary", desc: viewModel?.summary)
                        cell = titleCell
                    }
                } else { //instructors
                    
                }
            case .detailed:
                if let titleCell = tableView.dequeueReusableCell(withIdentifier: titleTextCellIdentifier, for: indexPath) as? TitleTextTableViewCell, let detailed = viewModel?.detailed {
                    let current = detailed[indexPath.row]
                    titleCell.bind(title: current.0, desc: current.1)
                    cell = titleCell
                }
            case .syllabus:
                guard let syllabusItems = viewModel?.syllabus, syllabusItems.count > indexPath.row else {
                    return cell
                }
                cell.textLabel?.text = syllabusItems[indexPath.row]
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard tableView.numberOfSections == 1 || section == 1 else {
            return nil
        }
        let headerView = Bundle.main.loadNibNamed("CourseSectionHeader", owner: nil, options: nil)?.first as? CourseSectionHeader
        headerView?.delegate = self
        headerView?.titleLabel.text = viewModel?.title
        headerView?.segmentedControl.selectedSegmentIndex = currentSegment.rawValue
        return headerView
    }
}

extension CourseDescriptionViewController: CourseSectionHeaderDelegate {
    func segmentedControllValueChanged(sender: CourseSectionHeader, segmentedControll: UISegmentedControl) {
        
        if let segment = CourseSegments(rawValue: segmentedControll.selectedSegmentIndex) {
            currentSegment = segment
            tableView.reloadSections(IndexSet(integer: tableView.numberOfSections - 1), with: .automatic)
        }
    }
}
