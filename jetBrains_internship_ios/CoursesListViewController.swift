//
//  ViewController.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 11/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import UIKit

struct CoursesListViewModel {
    let courses: [CourseShortViewModel]
    let hasNextPage: Bool
}

struct CourseShortViewModel: Equatable {
    let id: Int
    let title: String
    let description: String
    let cover: URL?
    let startDate: Date?
    let endDate: Date?
    
    // MARK: Equatable
    static func ==(lhs: CourseShortViewModel, rhs: CourseShortViewModel) -> Bool {
        return lhs.id == rhs.id && lhs.title == rhs.title && lhs.description == rhs.description && lhs.cover == rhs.cover && lhs.startDate == rhs.startDate && lhs.endDate == rhs.endDate
    }
}

protocol CoursesListViewProtocol: class {
    func display(coursesModels: CoursesListViewModel)
    func update(coursesModels: CoursesListViewModel, insertions: [IndexPath], deletions: [IndexPath], modifications: [IndexPath])
}

class CoursesListViewController: UIViewController, CoursesListViewProtocol {
    
    var presenter: CoursesListPresenterProtocol?
    var coursesModels: CoursesListViewModel?
    
    private let tableView = UITableView()
    private let refreshControl = UIRefreshControl()
    internal let courseCellIdentifier = "course_cell"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "CourseTableViewCell", bundle: nil), forCellReuseIdentifier: courseCellIdentifier)
        
        presenter?.requestCoursesNextPage()
    }
    
    private func setupView() {
        view.addSubview(tableView)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        tableView.separatorStyle = .none
        
//        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController
        
        // navBar
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = UIColor.stepikGreen
        navigationController?.navigationBar.isTranslucent = false
        
        navigationItem.title = "Courses"
    }
    
    func refresh(sender: AnyObject) {
        presenter?.refreshCourses()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: CoursesListViewProtocol
    func display(coursesModels: CoursesListViewModel) {
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        self.coursesModels = coursesModels
        tableView.reloadData()
    }
    
    func update(coursesModels: CoursesListViewModel, insertions: [IndexPath], deletions: [IndexPath], modifications: [IndexPath]) {
        self.coursesModels = coursesModels
        tableView.reloadData()
    }
}

extension CoursesListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let model = coursesModels, indexPath.row == model.courses.count - 5, model.hasNextPage {
            presenter?.requestCoursesNextPage()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let courses = coursesModels?.courses else {
            return
        }
        
        let courseDescriptionController = CourseDescriptionBuilder.buildControllerForDefault(with: courses[indexPath.row].id)
        navigationController?.pushViewController(courseDescriptionController, animated: true)
    }
}

extension CoursesListViewController: UITableViewDataSource {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let courses = coursesModels?.courses else {
            errorPrint("\(#function): error while creating course cell")
            return UITableViewCell()
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: courseCellIdentifier, for: indexPath)
        if let courseCell = cell as? CourseTableViewCell {
            courseCell.bind(viewModel: courses[indexPath.row])
        }
        return cell
    }

    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let courses = coursesModels?.courses else {
            return 0
        }
        return courses.count
    }
}
