//
//  Course.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 13/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import ObjectMapper
import RealmSwift
import Realm

class CoursesDto: Mappable {
    var courses: [Course]?
    var meta: MetaData?
    
    required init?(map: Map) {
        
    }
    
    //MARK: Mappable
    func mapping(map: Map) {
        courses <- map["courses"]
        meta    <- map["meta"]
    }
}

struct MetaData: Mappable {
    var page: Int = 0
    var hasNext: Bool = false
    var hasPrevious: Bool = false
    
    init?(map: Map) {
        
    }
    
    //MARK: Mappable
    mutating func mapping(map: Map) {
        page <- map["page"]
        hasNext <- map["has_next"]
        hasPrevious <- map["has_previous"]
    }
}

class Url: Object, Mappable {
    dynamic var quality: Int = 0
    dynamic var url: String?
    
    required init?(map: Map) {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }

    //MARK: Mappable
    func mapping(map: Map) {
        quality <- map["quality"]
        url     <- map["url"]
    }
}

class IntroVideo: Object, Mappable {
    dynamic var id: Int = 0
    dynamic var thumbnail: String?
    let urls = List<Url>()
    dynamic var status: String?
    dynamic var uploadDate: Date?
    dynamic var filename: String?
    
    required init?(map: Map) {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    //MARK: Mappable
    func mapping(map: Map) {
        id         <- map["id"]
        thumbnail  <- map["thumbnail"]
        status     <- map["status"]
        uploadDate <- map["upload_date"]
        filename   <- map["filename"]
        
        var mappedUrls: [Url]?
        mappedUrls <- map["urls"]
        urls.removeAll()
        for url in mappedUrls ?? [] {
            urls.append(url)
        }
    }
}

class Course: Object, Mappable {
    dynamic var id: Int = 0
    dynamic var summary: String?
    dynamic var summaryNoTags: String?
    dynamic var workload: String?
    dynamic var cover: String?
    var coverUrl: String? {
        guard let coverString = cover else { return nil}
        return StepikService.service.url + coverString
    }
    dynamic var intro: String?
    
    dynamic var courseFormat: String?
    dynamic var targetAudience: String?
    dynamic var certificateFooter: String?
    dynamic var certificateCoverOrg: String?
    dynamic var isCertificateAutoIssued: Bool = false
    dynamic var certificateRegularThreshold: Int = 0
    dynamic var certificateDistinctionThreshold: Int = 0
    dynamic var instructors: String?
    dynamic var certificate: String?
    dynamic var requirements: String?
    dynamic var requirementsNoTags: String?
    dynamic var desc: String?
    dynamic var descNoTags: String?
    dynamic var sections: String?
    dynamic var totalUnits: String?
    dynamic var enrollment: String?
    dynamic var isFavorite: Bool = false
    dynamic var action: String?
    dynamic var progress: String?
    dynamic var certificateLink: String?
    dynamic var certificateRegularLink: String?
    dynamic var certificateDistinctionLink: String?
    dynamic var scheduleLink: String?
    dynamic var scheduleLongLink: String?
    dynamic var firstDeadline: String?
    dynamic var lastDeadline: String?
    dynamic var subscription: String?
    dynamic var announcement: String?
    dynamic var isContest: Bool = false
    dynamic var isSelfPaced: Bool = false
    dynamic var isAdaptive: Bool = false
    dynamic var isIdeaCompatible: Bool = false
    dynamic var lastStep: String?
    dynamic var introVideo: IntroVideo?
    dynamic var socialProviders: String?
    dynamic var authors: String?
    dynamic var tags: String?
    dynamic var hasTutors: Bool = false
    dynamic var isPromoted: Bool = false
    dynamic var isEnabled: Bool = false
    dynamic var isProctored: Bool = false
    dynamic var proctorUrl: String?
    dynamic var previewSummary: String?
    dynamic var scheduleType: String = ""
    
    dynamic var certificatesCount: Int = 0
    dynamic var learnersCount: Int = 0
    
    dynamic var owner: String?
    dynamic var language: String?
    dynamic var isFeatured: Bool = false
    dynamic var isPublic: Bool = false
    dynamic var title: String?
    dynamic var slug: String?
    dynamic var beginDate: Date?
    dynamic var endDate: Date?
    dynamic var softDeadline: Date?
    dynamic var hardDeadline: Date?
    dynamic var gradingPolicy: String = ""
    dynamic var beginDateSource: Date?
    dynamic var endDateSource: Date?
    dynamic var softDeadlineSource: Date?
    dynamic var hardDeadlineSource: Date?
    dynamic var gradingPolicySource: String = ""
    dynamic var isAcitve: Bool = false
    dynamic var createDate: Date?
    dynamic var updateDate: Date?
    dynamic var learnersGroup: String?
    dynamic var testersGroup: String?
    dynamic var moderatorsGroup: String?
    dynamic var teachersGroup: String?
    dynamic var adminsGroup: String?
    dynamic var discussionsCount: Int = 0
    dynamic var discussion_proxy: String?
    dynamic var discussionThread: String?
    dynamic var ltiConsumerKey: String?
    dynamic var ltiSecretKey: String?
    
    required init?(map: Map) {
        super.init()
    }
    
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    //MARK: Mappable
    func mapping(map: Map) {
        id                              <- map["id"]
        summary                         <- map["summary"]
        summaryNoTags                   = summary?.noTagString
        workload                        <- map["workload"]
        cover                           <- map["cover"]
        intro                           <- map["intro"]
        courseFormat                    <- map["course_format"]
        targetAudience                  <- map["target_audience"]
        certificateFooter               <- map["certificate_footer"]
        certificateCoverOrg             <- map["certificate_cover_org"]
        isCertificateAutoIssued         <- map["is_certificate_auto_issued"]
        certificateRegularThreshold     <- map["certificate_regular_threshold"]
        certificateDistinctionThreshold <- map["certificate_distinction_threshold"]
        instructors                     <- map["instructors"]
        certificate                     <- map["certificate"]
        requirements                    <- map["requirements"]
        requirementsNoTags              = requirements?.noTagString
        desc                            <- map["description"]
        descNoTags                      = desc?.noTagString
        sections                        <- map["sections"]
        totalUnits                      <- map["total_units"]
        enrollment                      <- map["enrollment"]
        isFavorite                      <- map["is_favorite"]
        action                          <- map["actions"]
        progress                        <- map["progress"]
        certificateLink                 <- map["certificate_link"]
        certificateRegularLink          <- map["certificate_regular_link"]
        certificateDistinctionLink      <- map["certificate_distinction_link"]
        scheduleLink                    <- map["schedule_link"]
        scheduleLongLink                <- map["schedule_long_link"]
        firstDeadline                   <- map["first_deadline"]
        lastDeadline                    <- map["last_deadline"]
        subscription                    <- map["subscriptions"]
        announcement                    <- map["announcements"]
        isContest                       <- map["is_contest"]
        isSelfPaced                     <- map["is_self_paced"]
        isAdaptive                      <- map["is_adaptive"]
        isIdeaCompatible                <- map["is_idea_compatible"]
        lastStep                        <- map["last_step"]
        introVideo                      <- map["intro_video"]
        socialProviders                 <- map["social_providers"]
        authors                         <- map["authors"]
        tags                            <- map["tags"]
        hasTutors                       <- map["has_tutors"]
        isPromoted                      <- map["is_promoted"]
        isEnabled                       <- map["is_enabled"]
        isProctored                     <- map["is_proctored"]
        proctorUrl                      <- map["proctor_url"]
        previewSummary                  <- map["review_summary"]
        scheduleType                    <- map["schedule_type"]
        
        certificatesCount               <- map["certificates_count"]
        learnersCount                   <- map["learners_count"]
        
        owner                           <- map["owner"]
        language                        <- map["language"]
        isFeatured                      <- map["is_featured"]
        isPublic                        <- map["is_public"]
        title                           <- map["title"]
        slug                            <- map["slug"]
        beginDate                       <- (map["begin_date"], DateTransform())
        endDate                         <- (map["end_date"], DateTransform())
        softDeadline                    <- (map["soft_deadline"], DateTransform())
        hardDeadline                    <- (map["hard_deadline"], DateTransform())
        gradingPolicy                   <- map["grading_policy"]
        beginDateSource                 <- (map["begin_date_source"], DateTransform())
        endDateSource                   <- (map["end_date_source"], DateTransform())
        softDeadlineSource              <- (map["soft_deadline_source"], DateTransform())
        hardDeadlineSource              <- (map["hard_deadline_source"], DateTransform())
        gradingPolicySource             <- map["grading_policy_source"]
        isAcitve                        <- map["is_active"]
        createDate                      <- (map["create_date"], DateTransform())
        updateDate                      <- (map["update_date"], DateTransform())
        learnersGroup                   <- map["learners_group"]
        testersGroup                    <- map["testers_group"]
        moderatorsGroup                 <- map["moderators_group"]
        teachersGroup                   <- map["teachers_group"]
        adminsGroup                     <- map["admins_group"]
        discussionsCount                <- map["discussions_count"]
        discussion_proxy                <- map["discussion_proxy"]
        discussionThread                <- map["discussion_threads"]
        ltiConsumerKey                  <- map["lti_consumer_key"]
        ltiSecretKey                    <- map["lti_secret_key"]
    }
}
