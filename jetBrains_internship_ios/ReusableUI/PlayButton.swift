//
//  PlayButton.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 15/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import UIKit

class PlayButton: UIButton {
    var circleColor: UIColor = UIColor.stepikGreen {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var triangleColor: UIColor = .white {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        let diameter = min(rect.width, rect.height)
        context.setFillColor(circleColor.cgColor)
        context.addArc(center: CGPoint(x: rect.midX, y: rect.midY),
                       radius: diameter / 2.0,
                       startAngle: 0,
                       endAngle: 2 * CGFloat.pi, clockwise: true)
        context.fillPath()
        
        let quarterX = rect.midX / 2.0
        let quarterY = rect.midY / 2.0
        context.setFillColor(triangleColor.cgColor)
        context.move(to: CGPoint(x: quarterX * 1.2, y: quarterY))
        context.addLine(to: CGPoint(x: 3.2 * quarterX, y: rect.midY))
        context.addLine(to: CGPoint(x: quarterX * 1.2, y: 3 * quarterY))
        context.addLine(to: CGPoint(x: quarterX * 1.2, y: quarterY))
        context.fillPath()
    }
}
