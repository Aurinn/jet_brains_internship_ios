//
//  CustomLoging.swift
//  jetBrains_internship_ios
//
//  Created by Veronika Gaynetdinova on 12/05/2017.
//  Copyright © 2017 veronika. All rights reserved.
//

import Foundation

public func print(_ items: Any...) {
    Swift.print("✅", items)
}

public func debugPrint(_ items: Any...) {
    Swift.debugPrint("⚛️", items)
}

public func warningPrint(_ items: Any...) {
    Swift.debugPrint("⚠️", items)
}

public func errorPrint(_ items: Any...) {
    Swift.print("❌", items)
}
